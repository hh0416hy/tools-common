
# common-publics-tools
  提供了后台管理系统中常见的公共方法处理，包括格式化时间，格式化数据显示格式，计算时间间隔、验证表单等功能"
  * 方法仍在持续更新中，如需使用最新的包，
  gitee 同步更新，可在gitee上下载源码
  ****
  `https://gitee.com/hh0416hy/tools-common.git`
  ****
  `npm uninstall common-publics-tools`
  `npm install common-publics-tools`
#### 开始使用
`npm i common-publics-tools`
#### 全局引用
在vue项目中，main.js 中
`import {Html, NumberFn, Times, Verify} from 'common-publics-tools'`
`Vue.prototype.Html = Html`
##### 在child组件中使用
`this.$Html.copyText("复制")`
#### 局部引用
在vue组件中 中
`import {Html, NumberFn, Times, Verify} from 'common-publics-tools'`
##### 在child组件中使用
`Html.copyText("复制")`

## Html
  操作Html相关的公共方法
#### copyText
用于复制文字到剪切板
业务场景：点击当前链接复制文字
****
`Html.copyText("复制")`
##
参数|是否必须|type
-|-|---|
str|y|String
callback|n|Function
##
* 返回result
#####

#### findLocationUrlQuery
获取当前页面url后的所有参数
业务场景：页面中需要用url传参数的时候，常常需要用url携带参数
`Html.findLocationUrlQuery()`
****
* 返回result {}
#### findNumberByStr
找到字符串中的数字部分
业务场景：在项目中，某些素材在提交的时候，会按照图片序列提交。此时需要得到每个素材名称中的素材序列的顺序。
`Html.findNumberByStr(str)`
##
参数|是否必须|type
-|-|---|
str|y|String
##
* 返回 String 
#### downloadByUrl
通过下载链接下载excel文件
业务场景：在项目中，后端提供一个下载链接，可通过a标签可直接下载
 `Html.downloadByUrl(url)`
##
参数|是否必须|type|备注
-|-|---|-|
url|y|String|接口链接
params|n|Object|下载传参

#### removeHtmltag 
去除字符串中的html标签
 `Html.removeHtmltag(str)`
##
参数|是否必须|type|备注
-|-|---|-|
str|y|String|
* 返回去除了html的标签的字符串

#### getQueryString 
获取当前页面url上的参数
 `Html.getQueryString(id)`
##
参数|是否必须|type|备注
-|-|---|-|
value|y|String|参数名称

* 返回匹配到的参数

#### injectScript
动态引入js
 `Html.injectScript(url)`
##
参数|是否必须|type|备注
-|-|---|-|
src|y|String|js文件

* 
 #### getRpx
 获取px 和rpx之间的比例
 ***
 `Html.getRpx()`
 ##
* 返回比例值

## Times
  时间的相关公共方法
#### handlerUpdateTime
用于计算时间早于当前时间多久 或者 晚于当前时间多久。 
业务场景：计算某个任务执行于3天之前，计算某个定时任务3小时后执行。
`Times.handlerUpdateTime('2022-11-15', false, false)`
##
参数|是否必须|type|备注-
-|-|-|-
time|y|String|用于计算的时间
isNosmallerDay|n|Boolean|当计算结果少于一天是否需要计算，有时，小于一天的不在需要计算则返回今天，默认需要参与计算
isUTC|n|Boolean|是否按照UTC的时间进行计算
****
* 返回result，字符串 '2个月前' '1天前'
#### getlastdays
用于当前日期最近7天、14天、30天，本月、上月、上周 
业务场景：计算某个任务执行于3天之前，计算某个定时任务3小时后执行。
本周c-7，本月c-30， 本年c-365，最近7天、14天、30天、60天、90天、180天、d-num，上周l-7，上月l-30, 去年l-365
`Times.getlastdays('c-30')`
##
参数|是否必须|type|备注-
-|-|-|-
str|y|String|用于计算的时间c-7， c-30
****
* 返回result，Array, 返回[开始时间, 结束时间]

#### restTime
用于计算区间时间间隔天数
参数|是否必须|type|备注-
-|-|-|-
nowDate|y|String|开始时间
date|y|String|结束时间
****
* 返回相隔天数
#### getweekday
根据日期返回星期几
参数|是否必须|type|备注-
-|-|-|-
date|y|String|时间
****
* 返回“周几”

---------------------------------------------------------
## NumberFn
格式化数据的公共方法
#### compare
给数组对象排序
业务场景：接口一次性返回所有的数据，前端需要根据选中的字段进行排序，常用语表格排序
`arr.sort(NumberFn.compare('value', false))`
##
参数|是否必须|type|备注-
-|-|-|-
attr|y|String|用于排序的字段
rev|n|Boolean|升序还是降序，默认为降序

****
* 返回result，Array, 返回[{age: 20, name: 'ls'}, {age: 18, name: 'zs}]#### compare
#### handlerNumberToBToM
业务场景:将大额数字处理成M，K，B，T
`NumberFn.handlerNumberToBToM(3232323.32232, 3)`
##
参数|是否必须|type|备注-
-|-|-|-
data|y|Number|需要处理的数字
d|n|Number|保留几位小数
****
* 返回字符串，类似于：3.23M

#### numToFixed
保留几位小数
`NumberFn.numToFixed()`
参数|是否必须|type|备注-
-|-|-|-
num|y|Number|需要处理的数字
n|n|Number|保留几位小数
****
* 返回字符串，类似于：2.33
 #### noRepeatArrOfObj
 `NumberFn.noRepeatArrOfObj()`
 对象数组去重
 参数|是否必须|type|备注-
-|-|-|-
array|y|Array|原始数组
key|n|String|根据key值去重
****
* 返回字符串，类似于：2.33
---------------------------------------------------------
## Verify
用于表单验证，以及其他的验证
#### minxTextLength
 `Verify.minxTextLength()`
不得小于XX个字符
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
min|n|Number|不得小于xx的位数
callback|n|Function|验证后的回调函数
****
* 返回function

#### validateThreshold
 `Verify.validateThreshold()`
0-100的数字验证
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回回调function

#### validateImpressions
 `Verify.validateImpressions()`
正整数字的验证
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回回调function

#### isLetterNumber
 `Verify.isLetterNumber()`
验证数字、字母、下划线
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回回调function

#### isUrl
 `Verify.isUrl()`
验证是否是网址
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回回调function

#### vertifyPassword
 `Verify.vertifyPassword()`
验证密码是否由8-20位之间的字母、数字、以及指定字符组成  .-!@#$%^*()
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回回调function

#### isMobile
 `Verify.isMobile()`
验证手机号是否正确
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isPhone
 `Verify.isPhone()`
验证电话号是否正确
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isString
 `Verify.isString()`
验证是否为字符串
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isNumber
 `Verify.isNumber()`
验证是否为数字
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isBoolean
 `Verify.isBoolean()`
验证是否为布尔值
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isFunction
 `Verify.isFunction()`
验证是否为函数
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isNull
 `Verify.isNull()`
验证是否为Null
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值


#### isUndefined
 `Verify.isUndefined()`
验证是否为Undefined
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isObj
 `Verify.isObj()`
验证是否为Object
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isArray
 `Verify.isArray()`
验证是否为Array
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isDate
 `Verify.isDate()`
验证是否为日期
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isRegExp
 `Verify.isRegExp()`
验证是否为正则
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isError
 `Verify.isError()`
验证是否为错误对象
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isSymbol
 `Verify.isSymbol()`
验证是否为Symbol函数
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isPromise
 `Verify.isPromise()`
验证是否为promise对象
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值


#### isSet
 `Verify.isSet()`
验证是否为Set对象
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isWeiXin
 `Verify.isWeiXin()`
验证是否为微信浏览器
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isDeviceMobile
 `Verify.isDeviceMobile()`
验证是否为移动端
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isQQBrowser
 `Verify.isQQBrowser()`
验证是否QQ浏览器
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isSpider
 `Verify.isSpider()`
验证是否为爬虫
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isIos
 `Verify.isIos()`
验证是否为IOS
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isPC
 `Verify.isPC()`
验证是否为PC端
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### isCardID
 `Verify.isCardID()`
验证身份证是否正确 
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值

#### elementIsVisibleInViewport
 `Verify.elementIsVisibleInViewport()`
验证是否在适口范围内
参数|是否必须|type|备注-
-|-|-|-
value|y|Number|验证值
callback|n|Function|验证后的回调函数
****
* 返回布尔值