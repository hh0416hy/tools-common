export default {
  /**
   * 两位字符以上
   * @param {*} value 
   * @param {*} callback 
   * @returns 
   */
  minxTextLength (value, min, callback) {
    const m = min ? min : 2
    if (!value || value.length <= m) {
      return callback(new Error(`不得少于${m}个字符`))
    }
    callback()
  },
  /**
   * 0-100的数字验证
   */
   validateThreshold (value, callback) {
    if (value) {
      let numValue = value * 1
      if (!isNaN(numValue)) {
        if (parseFloat(value) > 100 || parseFloat(value) < 0) {
          callback(new Error(`请输入0 - 100的数字`))
        } else {
          callback()
        }
      } else {
        callback(new Error(`请输入数字`))
      }
    } else {
      callback()
    }
  },
  /**
   * 正整数字的验证
   */
   validateImpressions (value, callback) {
    if (value) {
      let numValue = value * 1
      if (!isNaN(numValue)) {
        if (parseFloat(numValue) < 0) {
          callback(new Error(`请输入正整数字`))
        } else {
          callback()
        }
      } else {
        callback(new Error(`请输入数字`))
      }
    } else {
      callback()
    }
  },
  /**
   * 验证数字，字母， 下划线
   * @param {*} value 
   * @param {*} cb 
   * @returns 
   */
  isLetterNumber (value, cb) {
    let reg =  /^[a-zA-Z0-9_\u4e00-\u9fa5//]+$/;
    if (!value) {
      return cb(new Error('必填项'));
    }
    if (!reg.test(value)) {
      return cb(new Error('只能输入字母数字下划线'));
    }
      cb()
  },
  /**
   * 是否是网址，域名
   * @param {*} rule 
   * @param {*} value 
   * @param {*} cb 
   * @returns 
   */
  isUrl (value, cb) {
    let reg=/^(http(s)?:\/\/)\w+[^\s]+(\.[^\s]+){1,}$/ 
    if (!value) {
      return cb(new Error('必填项'));
    }
    if (!reg.test(value)) {
      return cb(new Error('请填写正确的网址'));
    }
    cb()
  },
  /**
   * @param val 密码 由8-20位之间的字母、数字、以及指定字符组成  .-!@#$%^*()
   * @param {*} value 
   * @param {*} cb 
   * @returns 
   */
  vertifyPassword (value, cb) {
    let reg =/^(?=.*[A-Z])(?=.*[a-z])(?=.*[\-\.!@#\$%\^&\*\(\)])[0-9a-zA-Z\-\.!@#\$%\^&\*\(\)]{7,19}$/
    if (!value) {
      return cb(new Error('必填项'));
    }
    if (!reg.test(value)) {
      return cb(new Error('请按提示输入密码'));
    }
      cb()
  },
  /**
   * 验证手机号是否正确
   * @param {*} value 
   * @returns 
   */
  isMobile (value) {
    return /^1[0-9]{10}$/.test(value)
  },
  /**
   * 验证电话号码是否正确
   * @param {*} value 
   * @returns 
   */
  isPhone (value) {
    return /^([0-9]{3,4}-)?[0-9]{7,8}$/.test(value)
  },
  /**
   * 验证是否是字符串
   * @param {*} value 
   * @returns 
   */
  isString (value) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'String'
  },
  /**
   * 验证是否为数字
   * @param {*} value 
   * @returns 
   */
  isNumber (value) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Number'
  },
  /**
   * 验证是否为布尔值
   * @param {*} value 
   * @returns 
   */
  isBoolean (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'Boolean'
  },
  /**
   * 验证是否为函数
   * @param {*} value 
   * @returns 
   */
  isFunction (value) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Function'
  },
  /**
   * 验证是否为null
   * @returns 
   */
  isNull (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'Null'
  },
  /**
   * 验证是否为undefined
   * @param {*} value 
   * @returns 
   */
  isUndefined (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'Undefined'
  },
  /**
   * 验证是否为Object
   * @returns 
   */
  isObj (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'Object'
  },
  /**
   * 验证是否为数组
   * @param {*} value 
   * @returns 
   */
  isArray (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'Array'
  },
  /**
   * 验证是否为时间
   * @param {*} value 
   * @returns 
   */
  isDate (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'Date'
  },
  /**
   * 验证是否为正则
   * @param {*} value 
   * @returns 
   */
  isRegExp (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'RegExp'
  },
  /**
   * 验证是否为错误对象
   * @param {*} value 
   * @returns 
   */
  isError (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'Error'
  },
  /**
   * 是否为Symbol函数
   * @param {*} value 
   * @returns 
   */
  isSymbol (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'Symbol'
  },
  /**
   * 是否为promise对象
   * @param {*} value 
   * @returns 
   */
  isPromise (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'Promise'
  },
  /**
   * 是否为Set对象
   * @param {*} value 
   * @returns 
   */
  isSet (value) {
    return Object.prototype.toString.call(value).slice(8, -1) === 'Set'
  },
  /**
   * 是否为微信浏览器
   * @returns 
   */
  isWeiXin () {
    const ua = navigator.userAgent.toLowerCase();
    return ua.match(/microMessenger/i) == 'micromessenger'
  },
  /**
   * 是否为移动端
   * @returns 
   */
  isDeviceMobile () {
    const ua = navigator.userAgent.toLowerCase();
    return /android|webos|iphone|ipod|balckberry/i.test(ua)
  },
  /**
   * 是否为QQ浏览器
   * @returns 
   */
  isQQBrowser () {
    const ua = navigator.userAgent.toLowerCase();
    return !!ua.match(/mqqbrowser|qzone|qqbrowser|qbwebviewtype/i)
  },
  /**
   * 是否为爬虫
   * @returns 
   */
  isSpider () {
    const ua = navigator.userAgent.toLowerCase();
    return /adsbot|googlebot|bingbot|msnbot|yandexbot|baidubot|robot|careerbot|seznambot|bot|baiduspider|jikespider|symantecspider|scannerlwebcrawler|crawler|360spider|sosospider|sogou web sprider|sogou orion spider/.test(ua)
  },
  /**
   * 是否为IOS
   * @returns 
   */
  isIos () {
    var u = navigator.userAgent;
    if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {  //安卓手机
        return false
    } else if (u.indexOf('iPhone') > -1) {//苹果手机
        return true
    } else if (u.indexOf('iPad') > -1) {//iPad
        return false
    } else if (u.indexOf('Windows Phone') > -1) {//winphone手机
        return false
    } else {
        return false
    }
  },
  /**
   * 是否为PC端
   * @returns 
   */
  isPC () {
    var userAgentInfo = navigator.userAgent;
    var Agents = ["Android", "iPhone",
        "SymbianOS", "Windows Phone",
        "iPad", "iPod"];
    var flag = true;
    for (var v = 0; v < Agents.length; v++) {
        if (userAgentInfo.indexOf(Agents[v]) > 0) {
            flag = false;
            break;
        }
    }
    return flag;
  },
  /**
    * 验证是否为身份证
    * @returns 
    */
   isCardID (sId) {
    if (!/(^\d{15}$)|(^\d{17}(\d|X|x)$)/.test(sId)) {
      console.log('你输入的身份证长度或格式错误')
      return false
    }
    //身份证城市
    var aCity = { 11: "北京", 12: "天津", 13: "河北", 14: "山西", 15: "内蒙古", 21: "辽宁", 22: "吉林", 23: "黑龙江", 31: "上海", 32: "江苏", 33: "浙江", 34: "安徽", 35: "福建", 36: "江西", 37: "山东", 41: "河南", 42: "湖北", 43: "湖南", 44: "广东", 45: "广西", 46: "海南", 50: "重庆", 51: "四川", 52: "贵州", 53: "云南", 54: "西藏", 61: "陕西", 62: "甘肃", 63: "青海", 64: "宁夏", 65: "新疆", 71: "台湾", 81: "香港", 82: "澳门", 91: "国外" };
    if (!aCity[parseInt(sId.substr(0, 2))]) {
        console.log('你的身份证地区非法')
        return false
    }

    // 出生日期验证
    var sBirthday = (sId.substr(6, 4) + "-" + Number(sId.substr(10, 2)) + "-" + Number(sId.substr(12, 2))).replace(/-/g, "/"),
        d = new Date(sBirthday)
    if (sBirthday != (d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate())) {
        console.log('身份证上的出生日期非法')
        return false
    }

    // 身份证号码校验
    var sum = 0,
        weights = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2],
        codes = "10X98765432"
    for (var i = 0; i < sId.length - 1; i++) {
        sum += sId[i] * weights[i];
    }
    var last = codes[sum % 11]; //计算出来的最后一位身份证号码
    if (sId[sId.length - 1] != last) {
        console.log('你输入的身份证号非法')
        return false
    }

    return true
   },
   /**
    * 是否在适口范围内
    * @param {*} el 
    * @param {*} partiallyVisible 
    * @returns 
    */
    elementIsVisibleInViewport (el, partiallyVisible = false) {
      const { top, left, bottom, right } = el.getBoundingClientRect();
      const { innerHeight, innerWidth } = window;
      return partiallyVisible
          ? ((top > 0 && top < innerHeight) || (bottom > 0 && bottom < innerHeight)) &&
          ((left > 0 && left < innerWidth) || (right > 0 && right < innerWidth))
          : top >= 0 && left >= 0 && bottom <= innerHeight && right <= innerWidth;
   },
}
