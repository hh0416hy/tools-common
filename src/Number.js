export default {
  /**
   * 给数组对象排序
   * @param {*} attr 
   * @param {*} rev 生序还是降序，true升序，false降序
   * @returns 
   */
  compare (attr,rev) {
    if(rev ==  undefined){
          rev = 1;
      }else{
          rev = (rev) ? 1 : -1;
      }
      return (a,b) => {
          a = a[attr];
          b = b[attr];
            if(a < b){
              return rev * -1;
            }
            if(a > b){
                return rev * 1;
            }
          
        return 0;
      }
  },
  /**
   * @param {*} data 需要处理的数字
   * @param {*} d 保存小数点的位数
   * @returns 
   */
  handlerNumberToBToM (data, d) {
    let digits = d ? d : 1
    // 数字小鱼0的时候，也要处理
    let num = Number(data) < 0 ? Math.abs(Number(data)) : Number(data)
    let small = Number(data) < 0 ? '-' : ''
      var si = [
        { value: 1, symbol: "" },
        { value: 1E3, symbol: "K" },
        // { value: 1E4, symbol: "W" },
        { value: 1E6, symbol: "M" },
        { value: 1E9, symbol: "B" },
        { value: 1e+12, symbol: "T" },
      ];
      var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
      var i;
      for (i = si.length - 1; i > 0; i--) {
        if (num >= si[i].value) {
          break;
        }
      }
      let b = (num / si[i].value).toFixed(digits).replace(rx, "$1")
      return small+ Number(b).toLocaleString("en", { minimumFractionDigits: digits, maximumFractionDigits: digits }) +' '+si[i].symbol; 
  },
  /**
   * 保留n位小数
   * @param {*} num 
   * @param {*} n 
   * @returns 
   */
  numToFixed (num, n) {
    const _num = Number(num)
    const _n = Number(n)
    return _num.toFixed(n)
  },
  /**
   * 对象数组去重
   * @param {array} arr，需要去重的数组
   * @param {string} key，通过指定key值进行去重
   * @returns {array} 返回一个去重后的新数组
   */
  noRepeatArrOfObj(arr, key){
    const res = new Map();
    return arr.filter((item) => !res.has(item[key]) && res.set(item[key], true));
  },
  
}