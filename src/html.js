// 处理页面相关的方法
export default {
  // 复制文字到剪切板
  copyText (str, callback) {
    const input = document.createElement("input");
    document.body.appendChild(input);
    input.setAttribute("value", str);
    input.select();
    if (document.execCommand("copy")) {
      document.execCommand("copy");
    }
    if (callback) {
      callback()
    } else {
      alert('复制成功！可去粘贴使用')
    }
  },
  /**
   * 获取当前页面URL上的参数
   * @param {*} 
   * @returns 
   */
   findLocationUrlQuery() {
    const q = {};
    location.search.replace(/([^?&=]+)=([^&]+)/g,(_,k,v)=>q[k]=v);
    return JSON.stringify(q) !== '{}' ? q : ''
  },
  /**
     * 匹配找到字符串中的数字部分
     * @param {*} str
     */
   findNumberByStr (str) {
    let reg = /[0-9][0-9]*/g
    return str.match(reg)
  },
  /**
   * 通过下载链接下载excel文件
   * @param {*} url
   * @param {*} params
   */
   downloadByUrl (url, params) {
    let str = '?'
    if (params && JSON.stringify(params) !== '{}') {
      for (let k in params) {
        str += `${k}=${params[k]}&`
      }
    }
    let link = document.createElement('a')
      link.href = location.origin + url + paramsStr
      console.log(link.href, 'link.href')
      link.target = '_blank';
      // link.download = name
      link.click();
      setTimeout(() => {
        link.remove();
      }, 2000);
   },
   /**
    * 去除字符串中的html标签
    * @returns 
    */
   removeHtmltag (str) {
    return str.replace(/<[^>]+>/g, '')
   },
   /**
    * 获取url参数
    * @param {*} value 
    * @returns 
    */
   getQueryString (value) {
    const reg = new RegExp('(^|&)' + value + '=([^&]*)(&|$)', 'i');
    const search = window.location.search.split('?')[1] || '';
    const r = search.match(reg) || [];
    return r[2];
   },
   /**
    * 动态引入js
    * @param {*} src 
    */
   injectScript (src) {
    const s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = src;
    const t = document.getElementsByTagName('script')[0];
    t.parentNode.insertBefore(s, t);
   },
   /**
    * 获取px 与 rpx之间的比例
    * @returns 
    */
   getRpx () {
    let winWidth = wx.getSystemInfoSync().windowWidth;
    let rate = 750 / winWidth;
    if (!px && px != 0) {
      return rate;
    } else {
      return px * rate
    }
   }
}