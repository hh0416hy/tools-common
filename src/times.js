// 处理时间相关的方法
export default {
  /**
   *
   * @param {*} time 距离当前时间是过去的多少时间，1个月前，1年前，3天前
   * @param {*} smallerDay 小于一天是否参与计算
   * @param {*} isUtc 是否按照UTC时间计算，比北京时间，晚8个小时
   * @returns result 字符串形式
   */
  handlerUpdateTime(time, smallerDay, isUtc) {
    if (!time) return ''
    let dateTimeStamp = Date.parse(time.replace(/-/gi, '/'))
    var minute = 1000 * 60
    var hour = minute * 60
    var day = hour * 24
    var month = day * 30
    let year = month * 12
    var now = new Date().getTime()
    // UTC时间的计算需要减少8小时
    if (isUtc) now = now - 1000 * 60 * 60 * 8
    var diffValue = now - dateTimeStamp
    let suff = ''
    if (diffValue < 0) {
      diffValue = diffValue * -1
      suff = '后'
    } else {
      suff = '前'
    }
    var monthY = diffValue / year
    var monthC = diffValue / month
    var dayC = diffValue / day
    var hourC = diffValue / hour
    var minC = diffValue / minute
    let result = ''

    if (monthY >= 1) {
      result = '' + parseInt(monthY) + '年' + suff
    } else if (monthC >= 1) {
      result = '' + parseInt(monthC) + '个月' + suff
    }
    // else if(weekC>=1){
    //   result="" + parseInt(weekC) + "周前";
    // }
    else if (dayC >= 1) {
      result = '' + parseInt(dayC) + '天' + suff
    } else {
      if (!smallerDay) {
        // smallerDay 为true，则表示小于天的值不进行计算，默认为false
        if (hourC >= 1) {
          result = '' + parseInt(hourC) + '小时' + suff
        } else if (minC >= 0) {
          result = suff === '前' ? '1小时内' : parseInt(minC) + '分钟后'
        } else {
          result = suff === '前' ? '刚刚' : '1分钟内'
        }
      } else {
        result = '今天'
      }
    }

    return result
  },
  /**
   * 计算最近7、14、30天，本月，上月、最近60天，90，180天
   * @param {*} str
   * @returns
   */
  getlastdays(str) {
    let type = str.split('-')[0] // 类型，是本月c？还是最近多少天d，还是过去上周？l，
    let number = str.split('-')[1]
    let start = ''
    let dateList = []
    const end = new Date() // 今天
    if (type == 'c') {
      // 本月， 本周， 本年
      if (number == 7) {
        // 本周 7
        let week = end.getDay()
        let minusDay = week != 0 ? week - 1 : 6
        start = new Date(end.getTime() - minusDay * 1000 * 60 * 60 * 24)
        dateList = [formateDate(start), formateDate(end)]
      } else if (number == 30) {
        // 本月 30
        //获得当前月份0-11
        var currentMonth = end.getMonth()
        //获得当前年份4位年
        var currentYear = end.getFullYear()
        //求出本月第一天
        var firstDay = new Date(currentYear, currentMonth, 1)
        dateList = [formateDate(firstDay), formateDate(end)]
      } else if (number == 365) {
        // 本年 365
        //获得当前月份0-11
        var currentMonth = end.getMonth()
        //获得当前年份4位年
        var currentYear = end.getFullYear()
        //求出本月第一天
        var firstDay = new Date(currentYear, 1, 1)
        dateList = [formateDate(firstDay), formateDate(end)]
      }
    }
    if (type == 'dl') {
      // 过去多少天。不包含今天
      start = new Date()
      const day = Number(number)
      start.setTime(start.getTime() - 3600 * 1000 * 24 * day) // 从昨天往前的7天
      end.setTime(end.getTime() - 3600 * 1000 * 24 * 1) // 不包括今天
      dateList = [formateDate(start), formateDate(end)]
    }
    if (type === 'd') {
      // 最近多少天，包含今天
      start = new Date()
      start.setTime(start.getTime() - 3600 * 1000 * 24 * (Number(number) - 1)) // 从昨天往前的7天
      dateList = [formateDate(start), formateDate(end)]
    }
    if (type === 'l') {
      // 上周 ，上月
      if (number == 7) {
        // 上周
        const end = new Date() // 本周一到今天
        //返回date是一周中的某一天
        var week = end.getDay()
        //减去的天数
        var minusDay = week != 0 ? week - 1 : 6
        //获得当前周的第一天
        var currentWeekDayOne = new Date(
          end.getTime() - 1000 * 60 * 60 * 24 * minusDay
        )
        //上周最后一天即本周开始的前一天
        var priorWeekLastDay = new Date(
          currentWeekDayOne.getTime() - 1000 * 60 * 60 * 24
        )
        //上周的第一天
        var priorWeekFirstDay = new Date(
          priorWeekLastDay.getTime() - 1000 * 60 * 60 * 24 * 6
        )
        dateList = [
          formateDate(priorWeekFirstDay),
          formateDate(priorWeekLastDay),
        ]
      } else if (number == 30) {
        //获得当前月份0-11
        let currentMonth = end.getMonth()
        //获得当前年份4位年
        let currentYear = end.getFullYear()
        let currentDay = new Date(currentYear, currentMonth, 1)
        //上个月的第一天
        //年份为0代表,是本年的第一月,所以不能减
        if (currentMonth == 0) {
          currentMonth = 11 //月份为上年的最后月份
          currentYear-- //年份减1
        } else {
          currentMonth--
        }
        let firstDay = new Date(currentYear, currentMonth, 1)
        //求出上月的最后一天
        let lastDay = new Date(currentDay.getTime() - 1000 * 60 * 60 * 24)
        dateList = [formateDate(firstDay), formateDate(lastDay)]
      } else if (number == 365) {
        // 去年，或者千年
        //获得当前年份4位年
        let currentYear = end.getFullYear() - 1
        let firstDay = new Date(currentYear, 1, 1)
        //求出上月的最后一天
        let lastDay = new Date(currentYear, 12, 31)
        dateList = [formateDate(firstDay), formateDate(lastDay)]
      }
    }
    function formateDate(time) {
      let year = time.getFullYear()
      let month = time.getMonth() + 1
      let day = time.getDate()
      if (month < 10) {
        month = '0' + month
      }
      if (day < 10) {
        day = '0' + day
      }
      return year + '-' + month + '-' + day + ''
    }
    return dateList
  },
  /**
   * 日期区间间隔天数
   * @param {*} nowDate
   * @param {*} date
   * @returns
   */
  restTime(nowDate, date) {
    const setTime = new Date(date.replace(/-/g, '/'))
    const nowTime = new Date(nowDate.replace(/-/g, '/'))
    const restSec = setTime.getTime() - nowTime.getTime()
    const day = parseInt(Math.ceil(restSec / (60 * 60 * 24 * 1000)), 10)
    return day
  },
  /**
   * 根据日期返回星期几
   * @param {*} date
   * @returns
   */
  getweekday(date) {
    var weekArray = new Array(
      '周日',
      '周一',
      '周二',
      '周三',
      '周四',
      '周五',
      '周六'
    )
    var week = weekArray[new Date(date).getDay()]
    return week
  },
}
